﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using PersonConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonConnectionService.Services
{
    public class PersonConnectionServer : PersonConnections.PersonConnectionsBase
    {
        public int GetYear(string cnp)
        {
            int year;
            if (cnp[0] == '1' || cnp[0] == '2')
            {
                year = 1900;
            }
            else
            {
                year = 2000;
            }
            year += ((cnp[1] - '0') * 10 + (cnp[2] - '0'));
            return year;
        }

        public int GetMonth(string cnp)
        {
            int month;
            if (cnp[3] == 0)
            {
                month = cnp[4] - '0';
            }
            else
            {
                month = (cnp[3] - '0') * 10 + (cnp[4] - '0');
            }
            return month;
        }

        public int GetDay(string cnp)
        {
            int day;
            if (cnp[5] == 0)
            {
                day = cnp[6] - '0';
            }
            else
            {
                day = (cnp[5] - '0') * 10 + (cnp[6] - '0');
            }
            return day;
        }

        public int FindAge(string cnp)
        {
            int year = GetYear(cnp);
            int month = GetMonth(cnp);
            int day = GetDay(cnp);

            int age = DateTime.UtcNow.Year - year;

            if(DateTime.UtcNow.Month < month)
            {
                age--;
            }
            else
            {
                if(DateTime.UtcNow.Month == month && DateTime.UtcNow.Day < day)
                {
                    age--;
                }
            }

            return age;
        }

        public Person.Types.Gender FindGender(string cnp)
        {
            Person.Types.Gender gender;
            int s = cnp[0] - '0';
            if (s == 1 || s == 5)
            {
                gender = Person.Types.Gender.Male;
            }
            else
            {
                gender = Person.Types.Gender.Female;
            }

            return gender;
        }

        public override Task<LogResponse> LogOut(Person request, ServerCallContext context)
        {
            Console.WriteLine();
            Console.WriteLine(request.Name.ToUpper() + " HAS DISCONNECTED");
            Console.WriteLine();
            return Task.FromResult(new LogResponse() { Status = LogResponse.Types.Status.Disconnected });
        }

        public override Task<LogResponse> GetPersonInformation(Person personConnected, ServerCallContext context)
        {
            personConnected.Age = FindAge(personConnected.Cnp);
            personConnected.Gender = FindGender(personConnected.Cnp);
            Console.WriteLine();
            Console.WriteLine("Name: " + personConnected.Name);
            Console.WriteLine("Age: " + personConnected.Age);
            Console.WriteLine("Gender: " + personConnected.Gender);
            Console.WriteLine();
            return Task.FromResult(new LogResponse() { Status = LogResponse.Types.Status.Connected });
        }

        public override Task<LogResponse> LogIn(LogInRequest request, ServerCallContext context)
        {
            Console.WriteLine();
            Console.WriteLine(request.Person.Name.ToUpper() + " HAS CONNECTED");
            Console.WriteLine();

            return Task.FromResult(new LogResponse() { Status = LogResponse.Types.Status.Connected});
        }
    }
}
