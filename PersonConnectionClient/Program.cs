﻿using Grpc.Net.Client;
using PersonConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonConnectionClient
{
    class Program
    {
        public static bool IsCNPValid(string cnp)
        {
            List<char> lista = new List<char>(4) { '1', '2', '5', '6' };
            if (cnp.Length != 13)
            {
                return false;
            }
            for (int index = 0; index < cnp.Length; ++index)
            {
                if (cnp[index] < '0' || cnp[index] > '9')
                {
                    return false;
                }
            }
            if (!lista.Contains(cnp[0]) || cnp[3] > '1' || cnp[5] > '3')
            {
                return false;
            }
            return true;
        }

        static async Task Main(string[] args)
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new PersonConnections.PersonConnectionsClient(channel);

            Console.WriteLine("Name: ");
            var name = Console.ReadLine();

            Console.WriteLine();

            Console.WriteLine("CNP: ");
            var cnp = Console.ReadLine();

            while (!IsCNPValid(cnp))
            {
                Console.WriteLine();
                Console.WriteLine("CNP: ");
                cnp = Console.ReadLine();
            }

            var personToAdd = new Person()
            {
                Name = name,
                Cnp = cnp
            };

            var response = await client.LogInAsync(new LogInRequest { Person = personToAdd });

            Console.WriteLine();

            Console.WriteLine("Status: " + client.GetPersonInformation(personToAdd).Status.ToString());

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
            Console.WriteLine();
            Console.WriteLine("Status: " + client.LogOut(personToAdd).Status.ToString());
        }
    }
}
